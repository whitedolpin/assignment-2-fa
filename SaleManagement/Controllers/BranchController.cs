﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using SaleManagement.Models;
using SaleManagement.Service.IService;
using SaleManagement.Utility;

namespace SaleManagement.Controllers
{
    public class BranchController : Controller
    {
        private readonly IBranchService _branchService;
        public BranchController(IBranchService branchService)
        {
            _branchService = branchService;
        }
        public async Task<IActionResult> BranchIndex()
        {
            List<BranchDTO> list = new();

            ResponseDTO? response = await _branchService.GetAllBranchAsync();
            if (response != null && response.IsSuccess)
            {
                list = JsonConvert.DeserializeObject<List<BranchDTO>>(Convert.ToString(response.Result));
            }
            else
            {
                TempData["error"] = response?.Message;
            }

            return View(list);
        }

        public async Task<IActionResult> BranchCreate()
        {
            var selectLists = new List<SelectListItem>()
           {
               new SelectListItem {Text = SD.ActiveState,Value=SD.ActiveState },
               new SelectListItem {Text = SD.DeActiveState, Value=SD.DeActiveState },
           };
            ViewBag.State = selectLists;
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> BranchCreate(BranchDTO product)
        {

            if (ModelState.IsValid)
            {
                ResponseDTO? response = await _branchService.AddNewBranch(product);

                if (response != null && response.IsSuccess)
                {
                    TempData["success"] = "Product created successfully";
                    return RedirectToAction(nameof(BranchIndex));
                }
                else
                {
                    TempData["error"] = response?.Message;
                }
                var selectLists = new List<SelectListItem>()
           {
               new SelectListItem {Text = SD.ActiveState,Value=SD.ActiveState },
               new SelectListItem {Text = SD.DeActiveState, Value=SD.DeActiveState },
           };
                ViewBag.State = selectLists;
            }
            return View(product);
        }
        public async Task<IActionResult> DeleteBranch(string branchId)
        {
            ResponseDTO? response = await _branchService.GetBranchById(branchId);

            if (response != null && response.IsSuccess)
            {
                BranchDTO? model = JsonConvert.DeserializeObject<BranchDTO>(Convert.ToString(response.Result));
                return View(model);
            }
            else
            {
                TempData["error"] = response?.Message;
            }
            return NotFound();
        }
        [HttpPost]
        public async Task<IActionResult> DeleteBranch(BranchDTO branch)
        {
            ResponseDTO? response = await _branchService.DeleteBranch(branch.BranchId);
            if (response != null && response.IsSuccess)
            {
                TempData["success"] = "Product deleted successfully";
                return RedirectToAction(nameof(BranchIndex));
            }
            else
            {
                TempData["error"] = response?.Message;
            }
            return View(branch);
        }
		public async Task<IActionResult> BranchEdit(string branchId)
		{
			ResponseDTO? response = await _branchService.GetBranchById(branchId);
            var selectLists = new List<SelectListItem>()
           {
               new SelectListItem {Text = SD.ActiveState,Value=SD.ActiveState },
               new SelectListItem {Text = SD.DeActiveState, Value=SD.DeActiveState },
           };
            ViewBag.State = selectLists;
            if (response != null && response.IsSuccess)
			{
				BranchDTO? model = JsonConvert.DeserializeObject<BranchDTO>(Convert.ToString(response.Result));
				return View(model);
			}
			else
			{
				TempData["error"] = response?.Message;
			}
            
            return NotFound();
		}
		[HttpPost]
		public async Task<IActionResult> BranchEdit(BranchDTO branch)
		{
			ResponseDTO? response = await _branchService.UpdateBranch(branch);
			if (response != null && response.IsSuccess)
			{
				TempData["success"] = "Product updated successfully";
				return RedirectToAction(nameof(BranchIndex));
			}
			else
			{
				TempData["error"] = response?.Message;
			}
            var selectLists = new List<SelectListItem>()
           {
               new SelectListItem {Text = SD.ActiveState,Value=SD.ActiveState },
               new SelectListItem {Text = SD.DeActiveState, Value=SD.DeActiveState },
           };
            ViewBag.State = selectLists;
            return View(branch);
		}
	}
}
