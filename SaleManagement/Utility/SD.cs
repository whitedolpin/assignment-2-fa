﻿namespace SaleManagement.Utility
{
    public class SD
    {
        public static string BranchAPI { get; set; }
        public const string ActiveState = "Active";
        public const string DeActiveState = "DeActive";
        public enum ApiType
        {
            GET,
            POST,
            PUT,
            DELETE
        }
    }
}
